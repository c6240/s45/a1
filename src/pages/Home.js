
import { Fragment } from "react"
import AppNavbar from "./../components/AppNavbar"
import Banner from "./../components/Banner"
import Footer from "./../components/Footer"
import CourseCard from "./../components/CourseCard"


export default function Home(){
    return(
        <Fragment>
            <AppNavbar/>
            <Banner/>
            <CourseCard/>
            <Footer/>
        </Fragment>
    )
} 