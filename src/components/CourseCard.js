import { Card, Button } from "react-bootstrap";


export default function CourseCard(){
    return(
    <Card style={{ width: '18rem' }}>
    <Card.Body>
        <Card.Title>Sample Course</Card.Title>
        <Card.Text><h4>Description:</h4><p>This is a sample course offering.</p></Card.Text>
        <Card.Text><h4>Price:</h4><p>PHP 40,000</p></Card.Text>
        <Button variant="primary">Enroll</Button>
    </Card.Body>
    </Card>
    )
}